const apiUrl = 'http://localhost:8090/'
const { log } = console


document.addEventListener("DOMContentLoaded", async () => {


    const checkboxes = document.querySelectorAll('input[type="checkbox"]')
    const edit_btn = document.querySelectorAll('#edit-blog-btn')
    checkboxes.forEach(checkbox => {
        log('check visibility :', checkbox.style.visibility)
        checkbox.style.visibility = 'hidden'
        checkbox.addEventListener('click', (e) => {
            const checkboxes = document.querySelectorAll('input[type="checkbox"]');
            const checkedIDs = []
            checkboxes.forEach(c => {
                if (c.checked) checkedIDs.push(c.id)
            })
            if (checkedIDs?.length > 0) {
                alert(checkedIDs.join(','))
                localStorage.setItem('selectedBlogIds', JSON.stringify(checkedIDs))
                document.getElementById('delete-blogs-btn').style.visibility = 'visible'
            } else {
                document.getElementById('delete-blogs-btn').style.visibility = 'hidden'
            }
        })
    });

    document.getElementById('edit-blogs-btn').addEventListener('click', (e) => {
        log('edit btn click')
        checkboxes.forEach(checkbox => {
            log('visible :', checkbox.style.visibility)
            if (checkbox.style.visibility === 'hidden') {
                checkbox.style.visibility = 'visible'
                edit_btn.forEach(c => c.style.visibility = 'hidden')
            } else {
                checkbox.style.visibility = 'hidden'
                edit_btn.forEach(c => c.style.visibility = 'visible')
            }
        })
    })

    document.getElementById('delete-blogs-btn').addEventListener('click', async () => {
        try {
            let ids = JSON.parse(localStorage.getItem('selectedBlogIds'))
            await fetch(apiUrl + 'blog', {
                method: 'DELETE',
                headers: { "Content-Type": "application/json", authorization: `Bearer ${token}` },
                body: JSON.stringify({ ids })
            })
                .then(e => {
                    if (e?.status === 200) {
                        alert('deleted successfully')
                        location.reload()
                    }
                })
        } catch (e) {

        }
    })

    let token = localStorage.getItem('userToken')
    if (!token) location.href = '/'
    else {
        await fetch(apiUrl + 'user/profile', {
            method: 'GET',
            headers: { "Content-Type": "application/json", authorization: `Bearer ${token}` },
        }).then(e => e.json())
            .then(e => {
                document.getElementById('profile-pic').src = e?.data?.profile ? e?.data?.profile : '/images/profile_default.png'
                document.getElementById('user-name').value = e?.data?.name ? e?.data?.name : ''
                document.getElementById('user-mobile').value = e?.data?.mobile ? e?.data?.mobile : ''
                document.getElementById('user-email').value = e?.data?.email ? e?.data?.email : ''
                e?.data?.blogs.map((e, i) => {
                    // log('e :', e)
                    let div = document.createElement('div')
                    div.innerHTML = `
                    <div class="one-blog-container">
                    <img src="${e.image}" class="blog-img">
                    <div class="content-section">
                        <div class="section">
                            <h3 class="blog-title">${e.title}</h3>
                            <a href='/pages/blogpost.html?id=${e._id}'><i class='fa fa-edit edit-blog-btn'></i></a>
                        </div>
                        <div class="section">
                            <p class="blog-content">${e.content}</p>
                            <input type="checkbox" id=${e._id}>
                        </div>
                    </div>
                    <p class="blog-posted-date">${new Date(e.createdAt).toLocaleDateString()}</p>
                    </div>`
                    document.getElementById('blog-item').appendChild(div)
                })
            })
        let a = document.createElement('a')
        a.style = 'display: flex; align-items: center;'
        a.innerText = 'Logout'
        a.onclick = () => {
            if (window.confirm('Are You Surely Want To Logout')) {
                localStorage.clear()
                location.href = '/'
            }
        }
        let li = document.createElement('li')
        li.appendChild(a)
        // document.getElementById('login-section').appendChild(li)
        // getAllBlogs()
    }
})