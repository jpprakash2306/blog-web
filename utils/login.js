
const apiUrl = 'http://localhost:8090/'
const { log } = console

document.addEventListener("DOMContentLoaded", () => {
    if (localStorage.getItem('userToken')) {
        location.href = '/'
    }
    document.getElementById('login-btn').addEventListener('click', async () => {
        log('Login Btn Clicked')
        let userId = document.getElementById('user-id').value
        let password = document.getElementById('password').value
        if (!userId?.length || !password?.length) {
            return alert('Please Enter User Name and Password')
        }
        let obj = {
            userId,
            password
        }
        log('APi Called')
        try {
            await fetch(apiUrl + 'user/login', {
                method: 'POST',
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(obj)
            })
                .then(async res => {
                    let data = await res.json()
                    if (res.status === 200) {
                        localStorage.setItem('userToken', data.data)
                        window.location.href = '/'
                    } else
                        alert(data.msg)
                })
        } catch (e) {
            log('Error In Login', e)
        }
    })
})
