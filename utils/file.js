
const apiUrl = 'http://localhost:8090/'
const { log } = console

document.addEventListener("DOMContentLoaded", () => {
    document.getElementById('upload-btn').addEventListener('click', async (e) => {
        log('Upload Btn Clicked')
        var input = document.querySelector('input[type="file"]')
        if (!e.target) {
            return alert('Please Select A Picture')
        }
        let formData = new FormData()
        formData.append('profile', input.files[0])
        try {
            await fetch(apiUrl + 'file/profile', {
                method: 'POST',
                headers: {
                    authorization: `Bearer ${localStorage.getItem('userToken')}`
                },
                body: formData
            })
                .then(async res => {
                    let data = await res.json()
                    alert(data.msg)
                    if (res.status === 201) {
                        window.location.href = '/pages/login.html'
                    }
                })
        } catch (e) {
            log('Error In Register', e)
        }
    })
})
