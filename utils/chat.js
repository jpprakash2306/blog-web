document.addEventListener("DOMContentLoaded", () => {
    const socket = new WebSocket("ws://localhost:3000");

    const chatMessages = document.getElementById("chat-messages");
    const messageInput = document.getElementById("message-input");
    const sendButton = document.getElementById("send-button");

    socket.onmessage = (event) => {
        const message = JSON.parse(event.data);
        appendMessage(message);
    };

    sendButton.addEventListener("click", () => {
        const messageText = messageInput.value;
        if (messageText.trim() !== "") {
            const message = { text: messageText };
            socket.send(JSON.stringify(message));
            messageInput.value = "";
        }
    });

    function appendMessage(message) {
        const messageElement = document.createElement("div");
        messageElement.innerText = message.text;
        chatMessages.appendChild(messageElement);
    }
});
