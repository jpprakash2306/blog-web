document.addEventListener("DOMContentLoaded", () => {
    const createGroupForm = document.getElementById("create-group-form");
    const joinGroupForm = document.getElementById("join-group-form");

    createGroupForm.addEventListener("submit", (e) => {
        e.preventDefault();
        const groupName = document.getElementById("group-name").value;
        const groupPassword = document.getElementById("group-password").value;

        // Send a request to create a group
        fetch("http://localhost:3000/create-group", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ groupName, password: groupPassword })
        })
            .then(response => response.json())
            .then(data => {
                const groupId = data.groupId;
                // Redirect to the chat page with the group ID
                window.location.href = `/Web/chat.html?groupId=${groupId}`;
            })
            .catch(error => {
                console.error("Error creating group:", error);
            });
    });

    joinGroupForm.addEventListener("submit", (e) => {
        e.preventDefault();
        const groupId = document.getElementById("group-id").value;
        const joinPassword = document.getElementById("join-password").value;

        // Redirect to the chat page with the group ID
        window.location.href = `/chat.html?groupId=${groupId}&joinPassword=${joinPassword}`;
    });
});
