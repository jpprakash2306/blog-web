
const apiUrl = 'http://localhost:8090/'
const { log } = console

document.addEventListener("DOMContentLoaded", () => {
    if (localStorage.getItem('userToken')) {
        location.href = '/'
    }
    document.getElementById('terms').addEventListener('click', async e => {
        // log('terms :', document.getElementById('terms').value)
        if (document.getElementById('terms').value === 'on') {
            document.getElementById('terms').value = 'off'
            document.getElementById('register-btn').disabled = false
        } else {
            document.getElementById('terms').value = 'on'
            document.getElementById('register-btn').disabled = true
        }

    })
    document.getElementById('register-btn').addEventListener('click', async () => {
        log('register Btn Clicked')
        let userName = document.getElementById('user-name').value
        let mobile = document.getElementById('user-mobile').value
        let emailId = document.getElementById('user-email').value
        let password = document.getElementById('password').value
        let conf_password = document.getElementById('conf-password').value
        if (!userName?.length || !password?.length || !mobile?.length || !emailId?.length || !conf_password?.length) {
            return alert('Please Fill All Field')
        }
        if (password !== conf_password) {
            return alert('Password and Confirm Password Must Be Same')
        }
        let obj = {
            name: userName,
            mobile,
            email: emailId,
            password
        }
        log('APi Called')
        try {
            await fetch(apiUrl + 'user/register', {
                method: 'POST',
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(obj)
            })
                .then(async res => {
                    let data = await res.json()
                    alert(data.msg)
                    if (res.status === 201) {
                        window.location.href = '/pages/login.html'
                    }
                })
        } catch (e) {
            log('Error In Register', e)
        }
    })
})
