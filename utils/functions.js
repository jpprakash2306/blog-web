
const apiUrl = 'http://localhost:8090/'
const jsonToQuery = (json) => '?' + Object.keys(json).map(key => `${key}=${json[key]}`).join('&')

export default new Api()

class Api {

    async POST(url, payload) {
        await fetch(apiUrl + url, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(payload)
        }).then(res => res.json()).catch(e => console.log(`Error In POST: ${apiUrl + url}`))
    }

    async GET(url, payload) {
        await fetch(apiUrl + url + jsonToQuery(payload), {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            },
        }).then(res => res.json()).catch(e => console.log(`Error In GET: ${apiUrl + url}`))
    }

    async PUT(url, payload) {
        await fetch(apiUrl + url, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(payload)
        }).then(res => res.json()).catch(e => console.log(`Error In PUT: ${apiUrl + url}`))
    }

    async DELETE(url, array) {
        await fetch(apiUrl + url, {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ ids: array })
        }).then(res => res.json()).catch(e => console.log(`Error In DELETE: ${apiUrl + url}`))
    }

    async FILE(url, input, fileName) {
        var data = new FormData()
        data.append(fileName, input.files[0])
        await fetch(apiUrl + url, {
            method: 'POST',
            headers: {
                "Content-Type": "multipart/form-data"
            },
            body: data
        }).then(res => res.json()).catch(e => console.log(`Error In POST: ${apiUrl + url}`))
    }
}