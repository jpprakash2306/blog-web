
const apiUrl = 'http://localhost:8090/'
const { log } = console

document.addEventListener("DOMContentLoaded", async () => {
    let token = localStorage.getItem('userToken')
    if (!token) {
        document.getElementById('login-section').innerHTML = `
        <li><a href="/pages/login.html">Login</a></li>        
        <li><a href="/pages/register.html">Register</a></li>
        `
    } else {
        // let jwt = parseJwt(token)

        await fetch(apiUrl + 'user/profile', {
            method: 'GET',
            headers: { "Content-Type": "application/json", authorization: `Bearer ${token}` },
        }).then(e => e.json())
            .then(e => {
                let img = document.createElement('img')
                img.alt = 'Profile Pic'
                img.onclick = () => location.href = '/pages/profile.html'
                img.src = e?.data?.profile ? e?.data?.profile : '/images/profile_default.png'
                let li2 = document.createElement('li')
                li2.appendChild(img)
                document.getElementById('login-section').appendChild(li2)
            })
        let a = document.createElement('a')
        a.style = 'display: flex; align-items: center;'
        a.innerText = 'Logout'
        a.onclick = () => {
            if (window.confirm('Are You Surely Want To Logout')) {
                localStorage.clear()
                location.href = '/'
            }
        }
        let li = document.createElement('li')
        li.appendChild(a)
        document.getElementById('login-section').appendChild(li)
    }
    getAllBlogs()
})

async function getAllBlogs() {
    try {
        log('blog api called')
        let url = apiUrl + 'blog/'
        // url += jsonToQuery({})
        await fetch(url, {
            method: 'GET',
            headers: { "Content-Type": "application/json" },
        })
            .then(async res => {
                let data = await res.json()
                if (res.status === 200) {
                    if (data.data?.length) {
                        data.data.map((e, i) => {
                            let anchor = document.createElement('a')
                            anchor.href = `/pages/one-blog.html?id=${e._id}`
                            anchor.innerHTML = `
                            <img src="${e.image}" >
                            <b>${e.title}</b>
                            <p>${e.content}</p>
                            <hr>
                            <p>${new Date(e.createdAt).toLocaleDateString()}</p>`
                            document.getElementById('blog-container').appendChild(anchor)
                        })
                    } else {
                        let header = document.createElement('h1')
                        header.innerText = 'No Blogs Found'
                        document.getElementById('blog-container').appendChild(header)
                    }
                } else
                    alert(data.msg)
            })
    } catch (e) {
        log('Error In Get All Blogs', e)
    }
}

function parseJwt(token) {
    var base64Url = token.split('.')[1]
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
    var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(c => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)).join(''))
    return JSON.parse(jsonPayload)
}


const jsonToQuery = json => '?' + Object.keys(json).map(key => `${key}=${json[key]}`).join('&')