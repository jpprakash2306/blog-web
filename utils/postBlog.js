
const apiUrl = 'http://localhost:8090/'
const { log } = console
const jwtToken = localStorage.getItem('userToken')

document.getElementById('title').addEventListener('change', () => enableEditButton())
document.getElementById('sub-title').addEventListener('change', () => enableEditButton())
document.getElementById('content').addEventListener('change', () => enableEditButton())

document.getElementById('post-btn').addEventListener('click', () => {
    let blogId = localStorage.getItem('selectedBlogId')
    if (blogId) {
        log('Edit Clicked')
        EditOneBlog(blogId)
    } else
        log('Post Blog Clicked')
})

document.addEventListener("DOMContentLoaded", () => {
    document.getElementById('blog-img').style.visibility = 'hidden'
    let query = getQuery()
    if (query?.id) {
        getOneBlog(query?.id)
        document.getElementById('post-btn').innerText = 'Edit This Post'
        document.getElementById('post-btn').disabled = true
        // window.history.pushState({}, document.title, '/pages/blogpost.html')
    }
})

function getQuery() {
    let urlQuery = typeof window !== 'undefined' ? window.location.search.split("?")[1] : null;
    let query = {};
    if (urlQuery) {
        if (urlQuery.includes('&')) {
            let params = urlQuery.split('&');
            for (let i = 0; i < params.length; i++) {
                query[params[i].split('=')[0]] = params[i].split('=')[1];
            }
        } else {
            query[urlQuery.split('=')[0]] = urlQuery.split('=')[1];
        }
    }
    return query;
}

function enableEditButton() {
    document.getElementById('post-btn').disabled = false
}

async function getOneBlog(id) {
    try {
        log('blog api called')
        localStorage.setItem('selectedBlogId', id)
        let url = apiUrl + `blog/?id=${id}`
        await fetch(url, {
            method: 'GET',
            headers: { "Content-Type": "application/json" },
        })
            .then(async res => {
                let data = await res.json()
                if (res.status === 200) {
                    if (data.data?.length) {
                        let obj = data.data[0]
                        document.getElementById('title').value = obj.title
                        document.getElementById('sub-title').value = obj.subTitle
                        document.getElementById('content').value = obj.content
                        if (obj?.image) {
                            document.getElementById('blog-img').style.visibility = 'visible'
                            document.getElementById('blog-img').src = obj.image
                        }
                    } else {
                        let header = document.createElement('h1')
                        header.innerText = 'No Blogs Found'
                        document.getElementById('blog-container').appendChild(header)
                    }
                } else
                    alert(data.msg)
            })
    } catch (e) {
        log('Error In Get All Blogs', e)
    }
}

async function EditOneBlog(id) {
    localStorage.removeItem('selectedBlogId')
    try {
        let url = apiUrl + `blog/update/${id}`
        let obj = {
            title: document.getElementById('title').value,
            subTitle: document.getElementById('sub-title').value,
            content: document.getElementById('content').value,
            // image: ''
        }
        log('Payload of Edit', obj)
        await fetch(url, {
            method: 'PUT',
            headers: { "Content-Type": "application/json", authorization: `Bearer ${jwtToken}` },
            body: JSON.stringify(obj)
        })
            .then(async res => {
                let data = await res.json()
                if (res.status === 200) {
                    location.href = '/pages/profile.html'
                } else
                    alert(data.msg)
            })
    } catch (e) {
        log('Error In Edit One Blog', e)
    }
}